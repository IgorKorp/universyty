<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('main');

Route::get('/about', 'MainController@about')->name('about');

Route::get('/allstudents', 'StudentController@index')->name('allstudents');

Route::get('/topstudents', 'StudentController@topStudents')->name('topstudents');

Route::get('/topstudent', 'StudentController@topStudent')->name('topstudent');

Route::get('/groups', 'GroupController@index')->name('groups');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin','middleware'=>['web','auth']],function() {

    Route::get('/',['uses'=>'Admin\AdminController@show'])->name('admin.index');
    Route::get('/addstudent',['uses'=>'Admin\AdminAddStudentController@addStudent'])->name('admin.add.student');
    Route::post('/addstore',['uses'=>'Admin\AdminAddStudentController@addStore'])->name('admin.add.student.save');

    Route::get('/allstudents/{id}/edit', 'Admin\AdminEditStudentController@editStudent')->name('editstudent');
    Route::post('/allstudents/{id}/update', 'Admin\AdminEditStudentController@updateStudent')->name('updatestudent');
    Route::delete('/allstudent/{id}/delete', 'Admin\AdminEditStudentController@deleteStudent')->name('deletestudent');
}); 