
@extends ('layouts.index')

@section('intro')
<div id="intro">            
        <div class="intro-text">
          <div class="container">
            <div class="row">                   
              <div class="col-md-12">
                <div class="brand">
                  <h1><a href="{{route('main')}}">Главная страница</a></h1>
                
                </div>
              </div>
            </div>
          </div>
        </div>          
      </div>
@endsection
@section('content')
@guest
<div class="container">
  

  <div class="row">
    <div class="col-md-offset-2 col-md-8">
      <form  method="POST" action="{{ route('login') }}" role="form" class="contactForm">
        {{ csrf_field() }}
        <div class="form-group" {{ $errors->has('email') ? ' has-error' : '' }}>
          <input class="form-control" name="email" id="email" placeholder="Your Email" type="email" value="{{ old('email') }}" required autofocus>
          @if ($errors->has('email'))
              <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <input class="form-control" name="password" id="password" placeholder="Password" type="password" required>
          @if ($errors->has('password'))
              <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
          @endif
        </div>
        <div class="form-group">
          <div class="col-md-6 col-md-offset-4">
              <div class="checkbox">
                  <label>
                      <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                  </label>
              </div>
          </div>
      </div>
        <div class="text-center"><button type="submit" class="btn btn-theme btn-lg btn-block">Log in</button></div>
      </form>
    </div>
  </div>
 </div>
 @else
    

 
<form id="logout-form" action="{{ route('logout') }}" method="POST">
    {{ csrf_field() }}
    <div class="text-center">
      <input type="submit" class="btn btn-theme btn-lg btn-block" value="выход">
    </div>
</form>
      
@endguest


@endsection