
@extends ('layouts.index')

@section('intro')
<div id="intro">            
        <div class="intro-text">
          <div class="container">
            <div class="row">                   
              <div class="col-md-12">
                <div class="brand">
                <h1><a href="{{route('main')}}">Главная страница</a></h1>
                  <div class="line-spacer"></div>
                  <p><span>Страница с лучшими студентами</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>          
      </div>
    @endsection

@section ('content')
<table class="table">
                <thead>
                  <tr>
                    
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Отчество</th>
                    <th>Средняя оценка</th>
                  </tr>
                </thead>
                @foreach ($topStudents as $student)
                <tbody>
                  <tr>                    
                    <td>{{$student->LastName}}</td>
                    <td>{{$student->FirstName}}</td>
                    <td>{{$student->MidleName}}</td>
                    <td>{{$student->ASF}}</td>
                  </tr>
                </tbody>
                @endforeach
             
                {{-- <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                  </tr>
                </tbody> --}}
              </table>
@endsection


