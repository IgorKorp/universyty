
@extends ('layouts.index')

@section('intro')
<div id="intro">            
        <div class="intro-text">
          <div class="container">
            <div class="row">                   
              <div class="col-md-12">
                <div class="brand">
                <h1><a href="{{route('main')}}">Главная страница</a></h1>
                  <div class="line-spacer"></div>
                <p><span>Редактирование студента{{$student->id}}</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>          
      </div>
    @endsection

@section ('content')
@if (session('message'))
<div class="allert allert-success">
    {{ session('message') }}
</div>
@endif  

  <form method="post" action="{{ route('updatestudent', $student->id) }}"  class="contactForm">
    
    <input type="hidden" value="{{ csrf_token()}}" name="_token">
        <div class="form-group">
            <input name="LastName" class="form-control" id="LastName" placeholder="Фамилия" type="text" value="{{$student->LastName}}">
        </div>
        <div class="form-group">
            <input name="FirstName" class="form-control" id="FirstName" placeholder="Имя"  type="text" value="{{$student->FirstName}}">
        </div>
        <div class="form-group">
             <input name="MidleName" class="form-control" id="MidleName" placeholder="Отчество"  type="text" value="{{$student->MidleName}}">
        </div>
        <div class="form-group">
             <input name="Groups_id" class="form-control" id="Groups_id" placeholder="Группа"  type="list" value="{{$student->group_id}}">
        </div> 
        <div class="text-center">
            <input type="submit" class="btn btn-theme btn-lg btn-block" value="Сохранить">
        </div>
  </form>

@endsection


