
@extends ('layouts.index')

@section('intro')
<div id="intro">            
        <div class="intro-text">
          <div class="container">
            <div class="row">                   
              <div class="col-md-12">
                <div class="brand">
                <h1><a href="{{route('main')}}">Главная страница</a></h1>
                  <div class="line-spacer"></div>
                  <p><span>Страница со студентами</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>          
      </div>
    @endsection

@section ('content')
@if (session('message'))
<div class="allert allert-success">
    {{ session('message') }}
</div>
@endif  
<table class="table">
                <thead>
                  <tr>
                    <th>№</th>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Отчество</th>
                    <th>Группа</th>
                  </tr>
                </thead>
                @foreach ($allStudents as $student)
                <tbody>
                  <tr>
                    <th scope="row">{{$student->id}}</th>
                    <td>{{$student->LastName}}</td>
                    <td>{{$student->FirstName}}</td>
                    <td>{{$student->MidleName}}</td>
                    <td>{{$student->NameGroup}}</td>
                    <td>
                        <a href="{{ route('editstudent', $student->id) }}">
                          <img src="/img/glyphicons-31-pencil.png" width="14" height="14">
                        </a>                       
                    </td>
                    <td>
                      <form method="post" action="{{ route('deletestudent', $student->id) }}">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        <button><img src="/img/glyphicons-208-remove.png" width="14" height="14"></button>
                        
                      </form>                       
                    </td>
                   
                  </tr>
                </tbody>
                @endforeach

              </table>
<a href="{{ route('admin.add.student') }}" class="btn btn-theme btn-lg btn-block">Добавить студента</a>
@endsection


