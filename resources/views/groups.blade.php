@extends ('layouts.index')

@section('intro')
<div id="intro">            
        <div class="intro-text">
          <div class="container">
            <div class="row">                   
              <div class="col-md-12">
                <div class="brand">
                <h1><a href="{{route('main')}}">Главная страница</a></h1>
                  <div class="line-spacer"></div>
                  <p><span>Страница c группами</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>          
      </div>
    @endsection

@section ('content')
<table class="table">
                <thead>
                  <tr>
                    
                    <th>Название группы </th>
                    
                  </tr>
                </thead>
                 @foreach ($groups as $group)
                <tbody>
                  <tr>
                    
                    <td>{{$group->NameGroup}}</td>
              
                  </tr>
                </tbody>
                @endforeach
                {{-- <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    <td>@fat</td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    <td>@twitter</td>
                  </tr>
                </tbody> --}} 
              </table>
@endsection


