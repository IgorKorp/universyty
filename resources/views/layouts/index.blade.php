<!DOCTYPE html>
<html lang="en">
    <head>
        <title>ЩИ</title>
        <meta charset="utf-8-general-ci" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- css -->
        <link href={{ URL::asset('css/bootstrap.min.css') }} rel="stylesheet" media="screen">
        <link href={{ URL::asset('css/style.css') }} rel="stylesheet" media="screen">
        <link href={{ URL::asset('color/default.css') }} rel="stylesheet" media="screen">
        <script src= {{ URL::asset('js/modernizr.custom.js') }}></script>
       
      </head>
      <body>

        @section('navbar')
          <div class="menu-area">
            <div id="dl-menu" class="dl-menuwrapper">
              <button class="dl-trigger">Open Menu</button>
              <ul class="dl-menu">
                <li>
                  <a href="{{route('main')}}">Домашняя страница</a>
                </li>
                <li><a href="{{route('allstudents')}}">Все студенты</a></li>
                <li><a href="{{route('main')}}">Оценки студентов</a></li>
                <li><a href="{{route('groups')}}">Группы</a></li>
                <li><a href="{{route('about')}}">Contact</a></li>
                <li>
                  <a href="{{route('main')}}">Лучшие студенты</a>
                  <ul class="dl-submenu">
                    <li><a href="{{route('topstudent')}}">Лучший студент</a></li>
                    <li><a href="{{route('topstudents')}}">Топ 10 студентов</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <!-- /dl-menuwrapper -->
          </div>
          @show

          @yield('intro')
   
          
          @yield('content')
            
              
          @section('footer')
          <footer>
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <p>&copy; Mamba Theme. All Rights Reserved</p>
                  <div class="credits">
                    <!--
                      All the links in the footer should remain intact.
                      You can delete the links only if you purchased the pro version.
                      Licensing information: https://bootstrapmade.com/license/
                      Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Mamba
                    -->
                    <a href="https://bootstrapmade.com/">Free Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                  </div>
        
                </div>
              </div>
            </div>
        </footer>
        @show



        


        <script src={{ URL::asset('js/jquery.js') }}></script>
        <script src={{ URL::asset('js/bootstrap.min.js') }}></script>
        <script src={{ URL::asset('js/jquery.smooth-scroll.min.js') }}></script>
        <script src={{ URL::asset('js/jquery.dlmenu.js') }}></script>
        <script src={{ URL::asset('js/wow.min.js') }}></script>
        <script src={{ URL::asset('js/custom.js') }}></script>
        {{-- <script src={{ URL::asset('contactform/contactform.js') }}></script> --}}
     </body>
</html>