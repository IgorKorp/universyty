
@extends ('layouts.index')

@section('intro')
<div id="intro">            
        <div class="intro-text">
          <div class="container">
            <div class="row">                   
              <div class="col-md-12">
                <div class="brand">
                <h1><a href="{{route('main')}}">Главная страница</a></h1>
                  <div class="line-spacer"></div>
                  <p><span>Добавление нового студента</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>          
      </div>
    @endsection

@section ('content')
  @if (session('message'))
      <div class="allert allert-success">
          {{ session('message') }}
      </div>
  @endif   

  <form method="post" action="{{route('admin.add.student.save')}}"  class="contactForm">
    
    <input type="hidden" value="{{ csrf_token()}}" name="_token">
    <div class="form-group">
      <input name="LastName" class="form-control" id="LastName" placeholder="Фамилия" type="text">
      {{-- <div class="validation"></div> --}}
    </div>
    <div class="form-group">
        <input name="FirstName" class="form-control" id="FirstName" placeholder="Имя"  type="text">
        {{-- <div class="validation"></div> --}}
    </div>
    <div class="form-group">
        <input name="MidleName" class="form-control" id="MidleName" placeholder="Отчество"  type="text">
       {{--  <div class="validation"></div> --}}
    </div>
    <div class="form-group">
        <input name="group_id" class="form-control" id="group_id" placeholder="Группа"  type="text">
       {{--  <div class="validation"></div> --}}
    </div>

    <div class="text-center">
      <input type="submit" class="btn btn-theme btn-lg btn-block" value="Добавить">
    </div>
  </form>

@endsection


