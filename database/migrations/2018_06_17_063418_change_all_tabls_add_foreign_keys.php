<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAllTablsAddForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->foreign('group_id')->references('id')->on('groups')->onDelete('cascade')->change();
        });
       /*  Schema::table('users', function (Blueprint $table) {
            $table->foreign('teachers_id')->references('id')->on('teachers')->onDelete('cascade')->change();
        });*/
        
        Schema::table('groups', function (Blueprint $table) {
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade')->change();
        });
        Schema::table('relationsTeachAndSubs', function (Blueprint $table) {
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade')->change();
            $table->foreign('subject_id')->references('id')->on('subjects')->onDelete('cascade')->change();
           // $table->foreign('idSubject')->references('id')->on('subjects')->onDelete('cascade')->change();
           //$table->foreign('idTeacher')->references('id')->on('teachers')->onDelete('cascade')->change();
        });
        Schema::table('marks', function (Blueprint $table) {
            $table->foreign('student_id')->references('id')->on('students')->onDelete('cascade')->change();
            $table->foreign('rtas_id')->references('id')->on('relationsTeachAndSubs')->onDelete('cascade')->change();
           // $table->foreign('idSubject')->references('id')->on('subjects')->onDelete('cascade')->change();
           //$table->foreign('idTeacher')->references('id')->on('teachers')->onDelete('cascade')->change();
        });
        /* Schema::table('teachers', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->change();
        }); */
        Schema::table('role_user', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->change();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade')->change();
        }); 
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropForeign(['group_id']);
        });
        Schema::table('role_user', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['role_id']);
        });
    /*     Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['teachers_id']);
        }); */
         /* Schema::table('teachers', function (Blueprint $table) {
            $table->dropForeign(['users_id']);
        });  */
       
        Schema::table('groups', function (Blueprint $table) {
            $table->dropForeign(['teacher_id']);
        });
        Schema::table('relationsTeachAndSubs', function (Blueprint $table) {
            $table->dropForeign(['teacher_id']);
            $table->dropForeign(['subject_id']);
           
           
        });
        Schema::table('marks', function (Blueprint $table) {
            $table->dropForeign(['student_id']);
            $table->dropForeign(['rtas_id']);
    
        });
        

    }
}