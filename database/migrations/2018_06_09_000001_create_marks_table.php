<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marks', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('mark')->unsigned()->nullable();    
            $table->integer('rtas_id')->unsigned()->nullable();
            $table->integer('student_id')->unsigned();
         //   $table->foreign('idStudent')->references('id')->on('student')->onDelete('cascade');
        //    $table->integer('idSubject')->unsigned();
          //  $table->foreign('idSubject')->references('id')->on('subject')->onDelete('cascade');
         //   $table->integer('idTeacher')->unsigned();
           // $table->foreign('idTeacher')->references('id')->on('teacher')->onDelete('cascade');
            $table->timestamps();
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks');
    }
}
