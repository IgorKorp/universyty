<?php

use Illuminate\Database\Seeder;
use App\Group;

class GroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            $groups=[ [
                        'NameGroup' => "Прикладная математика",
                        'teacher_id' => 1,
                        
                    ],
                    [
                        'NameGroup' => "Мат. обеспечение и админ. информ. систем",
                        'teacher_id' => 3,
                        
                    ],
                    [
                        'NameGroup' => "Компьютерная безопасность",
                        'teacher_id' => 4,
                        
                    ],
                    [
                        'NameGroup' => "Фундаментальная математика",
                        'teacher_id' => 2,
                        
                    ]
                ];
            foreach($groups as $group) {
                Group::create($group);
            }

    }
   
}
