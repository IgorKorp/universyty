<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 1,
                'Name' => "Admin"
                
            ],
            [
                'id' => 2,
                'Name' => "Teacher"
                
            ],
            [
                'id' => 3,
                'Name' => "Student"
                
            ],
            [
                'id' => 4,
                'Name' => "guest"
                
            ]
            ];

        foreach($roles as $role){
            Role::create($role);
        }
    }
}
