<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'email' => "admin@mail.ru", 
                'password' => bcrypt('password')         
            ],
            [
                'id' => 2,
                'email' => "EEP@Tech.ru", 
                'password' =>'$2y$10$KFDfa5bcF7rYk5cdUi5/7.KLAeur8NYM5IOadxXBsqDVspllpNaN2'
                
            ],
            [
                'id' => 3,
                'email' => "BVP@Tech.ru", 
                'password' =>'$2y$10$KFDfa5bcF7rYk5cdUi5/7.KLAeur8NYM5IOadxXBsqDVspllpNaN2'
            ],
            [
                'id' => 4,
                'email' => "DVS@Tech.ru", 
                'password' =>'$2y$10$KFDfa5bcF7rYk5cdUi5/7.KLAeur8NYM5IOadxXBsqDVspllpNaN2'
            ],
            [
                'id' => 5,
                'email' => "SDI@Tech.ru", 
                'password' =>'$2y$10$KFDfa5bcF7rYk5cdUi5/7.KLAeur8NYM5IOadxXBsqDVspllpNaN2'
            ],
            [
                'id' => 6,
                'email' => "III@Tech.ru", 
                'password' =>'$2y$10$KFDfa5bcF7rYk5cdUi5/7.KLAeur8NYM5IOadxXBsqDVspllpNaN2'
            ],
            [
                'id' => 7,
                'email' => "SHDP@Tech.ru", 
                'password' =>'$2y$10$KFDfa5bcF7rYk5cdUi5/7.KLAeur8NYM5IOadxXBsqDVspllpNaN2'
            ],
            [
                'id' => 8,
                'email' => "VDN@Tech.ru", 
                'password' =>'$2y$10$KFDfa5bcF7rYk5cdUi5/7.KLAeur8NYM5IOadxXBsqDVspllpNaN2'
            ],
            ];

        foreach($users as $user){
            User::create($user);
        }
    }

        


    
}