<?php

use Illuminate\Database\Seeder;
use App\Mark;

class MarksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
    $marks = [
                [
                    'mark' => 4,
                    'student_id' => 1,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 3,
                    'student_id' => 1,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 3,
                    'student_id' => 1,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 2,
                    'student_id' => 1,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 5,
                    'student_id' => 1,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 3,
                    'student_id' => 1,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 4,
                    'student_id' => 1,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 4,
                    'student_id' => 1,
                    'RTAS_id' =>3
                ],// первыйй студент
                [
                    'mark' => 5,
                    'student_id' => 2,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 5,
                    'student_id' => 2,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 5,
                    'student_id' => 2,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 4,
                    'student_id' => 2,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 5,
                    'student_id' => 2,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 5,
                    'student_id' => 2,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 5,
                    'student_id' => 2,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 5,
                    'student_id' => 2,
                    'RTAS_id' =>3
                ],//второй студенты
                [
                    'mark' => 4,
                    'student_id' => 3,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 4,
                    'student_id' => 3,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 4,
                    'student_id' => 3,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 4,
                    'student_id' => 3,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 4,
                    'student_id' => 3,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 4,
                    'student_id' => 3,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 4,
                    'student_id' => 3,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 4,
                    'student_id' => 3,
                    'RTAS_id' =>3
                ],//3 студент 1 группы
                [
                    'mark' => 3,
                    'student_id' => 4,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 3,
                    'student_id' => 4,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 3,
                    'student_id' => 4,
                    'RTAS_id' =>1
                ],
                [
                    'mark' => 3,
                    'student_id' => 4,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 3,
                    'student_id' => 4,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 4,
                    'student_id' => 4,
                    'RTAS_id' =>2
                ],
                [
                    'mark' => 3,
                    'student_id' => 4,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 3,
                    'student_id' => 4,
                    'RTAS_id' =>3
                ],// 4 студент 1 группы
                [
                    'mark' => 4,
                    'student_id' => 5,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 5,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 5,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 5,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 5,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 5,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 5,
                    'RTAS_id' =>13
                ],
                [
                    'mark' => 4,
                    'student_id' =>5,
                    'RTAS_id' =>13
                ],// 1студент 2 группы
                [
                    'mark' => 4,
                    'student_id' => 6,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 6,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 6,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 6,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 6,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 6,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 6,
                    'RTAS_id' =>13
                ],
                [
                    'mark' => 4,
                    'student_id' =>6,
                    'RTAS_id' =>13
                ],//2 студент 2 группы
                [
                    'mark' => 3,
                    'student_id' => 7,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 3,
                    'student_id' => 7,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 3,
                    'student_id' => 7,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 7,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 7,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 3,
                    'student_id' => 7,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 7,
                    'RTAS_id' =>13
                ],
                [
                    'mark' => 5,
                    'student_id' =>7,
                    'RTAS_id' =>13
                ],//3 студент второй группы 
                [
                    'mark' => 3,
                    'student_id' => 8,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 8,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 4,
                    'student_id' => 8,
                    'RTAS_id' =>7
                ],
                [
                    'mark' => 3,
                    'student_id' => 8,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 8,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 8,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 5,
                    'student_id' => 8,
                    'RTAS_id' =>13
                ],
                [
                    'mark' => 3,
                    'student_id' =>8,
                    'RTAS_id' =>13
                ],//4 студент 2 группы
                [
                    'mark' => 4,
                    'student_id' => 9,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 4,
                    'student_id' => 9,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 4,
                    'student_id' => 9,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 4,
                    'student_id' => 9,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 4,
                    'student_id' => 9,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 4,
                    'student_id' => 9,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 4,
                    'student_id' => 9,
                    'RTAS_id' =>5
                ],
                [
                    'mark' => 4,
                    'student_id' => 9,
                    'RTAS_id' =>5
                ], // 1 студент 3 группы
                [
                    'mark' => 3,
                    'student_id' => 10,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 4,
                    'student_id' => 10,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 5,
                    'student_id' => 10,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 3,
                    'student_id' => 10,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 2,
                    'student_id' => 10,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 5,
                    'student_id' => 10,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 3,
                    'student_id' => 10,
                    'RTAS_id' =>5
                ],
                [
                    'mark' => 3,
                    'student_id' => 10,
                    'RTAS_id' =>5
                ], // 2 студент 3 группы
                [
                    'mark' => 4,
                    'student_id' => 11,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 5,
                    'student_id' => 11,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 3,
                    'student_id' => 11,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 4,
                    'student_id' => 11,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 3,
                    'student_id' => 11,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 3,
                    'student_id' => 11,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 3,
                    'student_id' => 11,
                    'RTAS_id' =>5
                ],
                [
                    'mark' => 3,
                    'student_id' => 11,
                    'RTAS_id' =>5
                ], // 3 студент 3 группы
                [
                    'mark' => 3,
                    'student_id' => 12,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 4,
                    'student_id' => 12,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 5,
                    'student_id' => 12,
                    'RTAS_id' =>3
                ],
                [
                    'mark' => 5,
                    'student_id' => 12,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 3,
                    'student_id' => 12,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 4,
                    'student_id' => 12,
                    'RTAS_id' =>6
                ],
                [
                    'mark' => 5,
                    'student_id' => 12,
                    'RTAS_id' =>5
                ],
                [
                    'mark' => 5,
                    'student_id' => 12,
                    'RTAS_id' =>5
                ], // 4 студент 3 группы
                [
                    'mark' => 4,
                    'student_id' => 13,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 4,
                    'student_id' => 13,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 4,
                    'student_id' => 13,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 4,
                    'student_id' => 13,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 4,
                    'student_id' => 13,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 4,
                    'student_id' => 13,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 4,
                    'student_id' => 13,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 13,
                    'RTAS_id' =>15
                ],//1 студент 4 группы
                [
                    'mark' => 4,
                    'student_id' => 14,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 4,
                    'student_id' => 14,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 4,
                    'student_id' => 14,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 4,
                    'student_id' => 14,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 4,
                    'student_id' => 14,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 4,
                    'student_id' => 14,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 4,
                    'student_id' => 14,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 4,
                    'student_id' => 14,
                    'RTAS_id' =>15
                ],//2 студент 4 группы
                [
                    'mark' => 3,
                    'student_id' => 15,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 4,
                    'student_id' => 15,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 3,
                    'student_id' => 15,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 4,
                    'student_id' => 15,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 5,
                    'student_id' => 15,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 3,
                    'student_id' => 15,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 3,
                    'student_id' => 15,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 2,
                    'student_id' => 15,
                    'RTAS_id' =>15
                ],//3 студент 4 группы
                [
                    'mark' => 5,
                    'student_id' => 16,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 5,
                    'student_id' => 16,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 5,
                    'student_id' => 16,
                    'RTAS_id' =>16
                ],
                [
                    'mark' => 5,
                    'student_id' => 16,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 5,
                    'student_id' => 16,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 5,
                    'student_id' => 16,
                    'RTAS_id' =>11
                ],
                [
                    'mark' => 5,
                    'student_id' => 16,
                    'RTAS_id' =>15
                ],
                [
                    'mark' => 5,
                    'student_id' => 16,
                    'RTAS_id' =>15
                ]//4 студент 4 группы
             ];

        foreach($marks as $mark){
            Mark::create($mark);
        }
    }
}
