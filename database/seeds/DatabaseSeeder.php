<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);      
        $this->call(UsersSeeder::class);
        $this->call(TeachersSeeder::class);
        $this->call(SubjectsSeeder::class);
        $this->call(GroupsSeeder::class);
        $this->call(StudentsSeeder::class);
        $this->call(RelationsTeachAndSubSeeder::class);
        $this->call(MarksSeeder::class); 
        $this->call(RoleUserSeeder::class);
                 
         //
    }
}
