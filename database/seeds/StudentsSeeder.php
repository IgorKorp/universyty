<?php

use Illuminate\Database\Seeder;
use App\Student;

class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::insert('INSERT INTO `students` (`LastName`, `FirstName`, `MidleName`, `group_id`) VALUES (?,?,?,?)', 
                                           [ 
                                                'Шорохов',
                                                'Алексей',
                                                'Николаевич',
                                                1

                                            ]);
        DB::insert('INSERT INTO `students` (`LastName`, `FirstName`, `MidleName`, `group_id`) VALUES (?,?,?,?)',
                                            [
                                                'Шилова',
                                                'Инна',
                                                'Олеговна',
                                                1
                                                
                                            ]);
        DB::insert('INSERT INTO `students` (`LastName`, `FirstName`, `MidleName`, `group_id`) VALUES (?,?,?,?)',
                                            [
                                                'Лужков',
                                                'Игнат',
                                                'Глебович',
                                                1
                                                
                                            ]);
        DB::insert('INSERT INTO `students` (`LastName`, `FirstName`, `MidleName`, `group_id`) VALUES (?,?,?,?)',
                                            [
                                                'Кипелов',
                                                'Андрей',
                                                'Вадимович',
                                                1
                                                
                                            ]);
           
        DB::table('students')->insert(
                                        [
                                            
                                            [
                                                'LastName' => "Прохоров",
                                                'FirstName' => "Максим",
                                                'MidleName' => "Сергеевич",
                                                'group_id' => 2
                                                
                                            ],
                                            [
                                                'LastName' => "Арбузов",
                                                'FirstName' => "Андрей",
                                                'MidleName' => "Вадимович",
                                                'group_id' => 2
                                                
                                            ],
                                            [
                                                'LastName' => "Дымкова",
                                                'FirstName' => "Людмила",
                                                'MidleName' => "",
                                                'group_id' => 2
                                                
                                            ],
                                            [
                                                'LastName' => "Шанина",
                                                'FirstName' => "Анна",
                                                'MidleName' => "Владимировна",
                                                'group_id' => 2
                                                
                                            ],
                                            [
                                                'LastName' => "Инкин",
                                                'FirstName' => "Владислав",
                                                'MidleName' => "Валерьевич",
                                                'group_id' => 3
                                                
                                            ],
                                            [
                                                'LastName' => "Инкина",
                                                'FirstName' => "Влада",
                                                'MidleName' => "Валерьевна",
                                                'group_id' => 3
                                                
                                            ],
                                            [
                                                'LastName' => "Дронов",
                                                'FirstName' => "Виктор",
                                                'MidleName' => "Игоревич",
                                                'group_id' => 3
                                                
                                            ],
                                            [
                                                'LastName' => "Шлякова",
                                                'FirstName' => "Нина",
                                                'MidleName' => "Петровна",
                                                'group_id' => 3
                                                
                                            ]
                                            
                                        ]
        );     
       
                $students=[ [
                                'LastName' =>" Петров",
                                'FirstName' => "Петр",
                                'MidleName' => "Петрович",
                                'group_id' => 4
                                
                            ],
                            [
                                'LastName' => "Уик",
                                'FirstName' => "Джон",
                                'MidleName' => "",
                                'group_id' => 4
                                
                            ],
                            [
                                'LastName' => "Рязанов",
                                'FirstName' => "Василий",
                                'MidleName' => "Дмитриевич",
                                'group_id' => 4
                                
                            ],
                            [
                                'LastName' => "Финикова",
                                'FirstName' => "София",
                                'MidleName' => "Матвеевна",
                                'group_id' => 4
                                
                            ]
                           ];
        foreach($students as $student) {
            Student::create($student);
        }

    }
}
