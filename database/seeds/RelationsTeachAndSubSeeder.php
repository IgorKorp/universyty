<?php

use Illuminate\Database\Seeder;
use App\RelationsTeachAndSub;

class RelationsTeachAndSubSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teachandsubs = [
                            [
                                'id' => 1,
                                'teacher_id' => '1',
                                'subject_id' => '1'
                            ],
                            [
                                'id' => 2,
                                'teacher_id' => '1',
                                'subject_id' => '8'
                            ],
                            [
                                'id' => 3,
                                'teacher_id' => '2',
                                'subject_id' => '2'
                            ],
                            [
                                'id' => 4,
                                'teacher_id' => '2',
                                'subject_id' => '6'
                            ],
                            [
                                'id' => 5,
                                'teacher_id' => '3',
                                'subject_id' => '4'
                            ],
                            [
                                'id' => 6,
                                'teacher_id' => '3',
                                'subject_id' => '11'
                            ],
                            [
                                'id' => 7,
                                'teacher_id' => '3',
                                'subject_id' => '1'
                            ],
                            [
                                'id' => 8,
                                'teacher_id' => '4',
                                'subject_id' => '10'
                            ],
                            [
                                'id' => 9,
                                'teacher_id' => '4',
                                'subject_id' => '11'
                            ],
                            [
                                'id' => 10,
                                'teacher_id' => '5',
                                'subject_id' => '12'
                            ],
                            [
                                'id' => 11,
                                'teacher_id' => '5',
                                'subject_id' => '5'
                            ],
                            [
                                'id' => 12,
                                'teacher_id' => '5',
                                'subject_id' => '7'
                            ],
                            [
                                'id' => 13,
                                'teacher_id' => '6',
                                'subject_id' => '9'
                            ],
                            [
                                'id' => 14,
                                'teacher_id' => '7',
                                'subject_id' => '3'
                            ],
                            [
                                'id' => 15,
                                'teacher_id' => '7',
                                'subject_id' => '7'
                            ],
                            [
                                'id' => 16,
                                'teacher_id' => '7',
                                'subject_id' => '2'
                            ]
                        ];
            foreach($teachandsubs as $teachandsub){
                RelationsTeachAndSub::create($teachandsub);
            }
    }
}
