<?php

use Illuminate\Database\Seeder;
use App\Teacher;

class TeachersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teachers = [
            [
                'id' => 1,
                'user_id' => "2",
                'LastName' => "Ерёменко",
                'FirstName' => "Евгений", 
                'MidleName' => "Павлович"
                
            ],
            [
                'id' => 2,
                'user_id' => "3",
                'LastName' => "Березин",
                'FirstName' => "Виктор", 
                'MidleName' => "Петрович"
                
            ],
            [
                'id' => 3,
                'user_id' => "4",
                'LastName' => "Дружин",
                'FirstName' => "Валентин",
                'MidleName' => "Семенович"
                
            ],
            [
                'id' => 4,
                'user_id' => "5",
                'LastName' => "Серебряков",
                'FirstName' => "Денис",
                'MidleName' => "Иванович"
                
            ],
            [
                'id' => 5,
                'user_id' => "6",
                'LastName' => "Иванов",
                'FirstName' => "Иван",
                'MidleName' => "Иванович"
                
            ],
            [
                'id' => 6,
                'user_id' => "7",
                'LastName' => "Шилов",
                'FirstName' => "Дмитрий",
                'MidleName' => "Петрович"
                
            ],
            [
                'id' => 7,
                'user_id' => "8",
                'LastName' => "Валенков",
                'FirstName' => "Дамир",
                'MidleName' => "Назарович"
                
            ],
            ];

        foreach($teachers as $teacher){
            Teacher::create($teacher);
        }


    }
}