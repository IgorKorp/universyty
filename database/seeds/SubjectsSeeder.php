<?php

use Illuminate\Database\Seeder;
use App\Subject;

class SubjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $subjects = [
                        [
                            'id' => 1,
                            'NameSubject' => "Дискретная математика"
                        ],
                        [
                            'id' => 2,
                            'NameSubject' => "Дифференциальные уравнения"
                        ],
                        [
                            'id' => 3,
                            'NameSubject' => "Функциональный анализ"
                        ],
                        [
                            'id' => 4,
                            'NameSubject' => "Теория вероятности"
                        ],
                        [
                            'id' => 5,
                            'NameSubject' => "Аналитическая геометрия"
                        ],
                        [
                            'id' => 6,
                            'NameSubject' => "Математическое моделирование"
                        ],
                        [
                            'id' => 7,
                            'NameSubject' => "Теоретическая механика"
                        ],
                        [
                            'id' => 8,
                            'NameSubject' => "Cpp"
                        ],
                        [
                            'id' => 9,
                            'NameSubject' => "Базы данных"
                        ],
                        [
                            'id' => 10,
                            'NameSubject' => "Информационная безопасность"
                        ],
                        [
                            'id' => 11,
                            'NameSubject' => "Шифрование данных"
                        ],
                        [
                            'id' => 12,
                            'NameSubject' => "Математический анализ"
                        ]
                    ];
        foreach($subjects as $subject){
            Subject::create($subject);
        }
    }
}
