<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = ['LastName', 'FirstName', 'MidleName', 'group_id'];
    
    public function group()
    {
        return $this->hasOne('App\Group');
    }
}
