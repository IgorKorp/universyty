<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model
{
    public function rtas()
    {
        return $this->hasOne('App\RelationsTeachAndSub');
    }
}
