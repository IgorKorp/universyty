<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\User;
use App\RoleUser;
//use Illuminate\Auth\Access\Gate;
use Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
            
         Gate::define('add-student', function(User $user){         
            foreach($user->roles as $role){
                if($role->name == 'Admin'){
                    return TRUE;
                }
            } 
            return FALSE;
        });

        Gate::define('update-student', function(User $user){         
            foreach($user->roles as $role){
                if($role->name == 'Admin'){
                    return TRUE;
                }
            } 
            return FALSE;
        });

        Gate::define('delete-student', function(User $user){         
            foreach($user->roles as $role){
                if($role->name == 'Admin'){
                    return TRUE;
                }
            } 
            return FALSE;
        });
           
           

       
    }
}
