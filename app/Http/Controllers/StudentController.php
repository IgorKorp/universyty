<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Group;
use DB;
use App\Quotation;

class StudentController extends Controller
{
    public function index()
    { 

        $allStudents = DB::select("SELECT students.id, students.LastName, students.FirstName, students.MidleName, groups.NameGroup FROM `students` JOIN groups ON students.group_id = groups.id ");
        return view('allstudents', compact('allStudents'));
        
    }
    
    public function topStudents()
    {
        $topStudents = DB::select("SELECT  students.LastName, students.FirstName, students.MidleName, AVG(marks.mark) ASF FROM students, marks WHERE students.id = marks.student_id GROUP BY marks.student_id  
        ORDER BY `ASF`  DESC LIMIT 10");
        return view('topstudents', compact('topStudents'));
    }

    public function topStudent()
    {
        $topStudent = DB::select("SELECT  
        students.LastName, 
        students.FirstName, 
        students.MidleName,
        groups.NameGroup,
        AVG(marks.mark) ASF 
        FROM students 
        JOIN marks ON students.id = marks.student_id 
        JOIN groups ON students.group_id = groups.id	
        GROUP BY marks.student_id , groups.id
        HAVING AVG(marks.mark) = (SELECT  AVG(marks.mark) gg FROM students, marks WHERE students.id = marks.student_id GROUP BY marks.student_id  
        ORDER BY  `gg` DESC   LIMIT 1  )");
        return view('topstudent', compact('topStudent'));
    }
    
  

}