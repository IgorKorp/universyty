<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\Group;
use Gate;
use DB;
use App\Quotation;


class AdminEditStudentController extends Controller
{
    public function editStudent($id)
    {
        $student = Student::find($id);
        return view('editstudent', ['student' => $student]);
    }

    public function updateStudent(Request $request, $id)
    {
          if(Gate::denies('update-student')){
              return redirect()->back()->with(['message' => 'У вас нет прав']);
        }  

        $student = Student::find($id);
        $student->fill($request->all());
        $student->save();

        return redirect()->route('allstudents');
    }

    public function deleteStudent($id)
    {
         if(Gate::denies('delete-student')){
            return redirect()->back()->with(['message' => 'У вас нет прав']);
        } 

        Student::find($id)->delete();

        return redirect()->route('allstudents');
    }




}


