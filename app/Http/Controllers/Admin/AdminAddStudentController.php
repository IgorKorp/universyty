<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;
use App\User;
use Gate;

class AdminAddStudentController extends Controller
{
    public function addStudent()
    {
        return view('addstudent');
    }
    public function addStore(Request $request)
    {
        if(Gate::denies('add-student')){
            return redirect()->back()->with(['message' => 'У вас нет прав']);
        }

        Student::create($request->all());
        return redirect()->route('allstudents');
    }
}
