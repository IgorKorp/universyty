<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use DB;
use App\Quotation;

class GroupController extends Controller
{
    public function index()
    {
        $groups = Group::all();
        return view('groups', compact('groups'));
    }
}
